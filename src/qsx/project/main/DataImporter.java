package qsx.project.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import org.hypergraphdb.HGHandle;
import org.hypergraphdb.HGLink;
import org.hypergraphdb.HGQuery.hg;
import org.hypergraphdb.HGValueLink;
import org.hypergraphdb.HyperGraph;
import org.hypergraphdb.query.AtomPartCondition;
import org.hypergraphdb.query.AtomTypeCondition;
import org.hypergraphdb.query.AtomValueCondition;
import org.hypergraphdb.query.ComparisonOperator;
import org.hypergraphdb.query.MapCondition;
import org.hypergraphdb.query.impl.DerefMapping;
import org.hypergraphdb.query.impl.LinkProjectionMapping;
import org.hypergraphdb.util.CompositeMapping;

import qsx.project.datatypes.Person;

public class DataImporter {
	
	public static void main(String[] args) {
		File file;
		String line;
		HyperGraph graph = new HyperGraph("/home/cata/development/qsx/gdb");
		try {
			file = new File("/home/cata/Desktop/sample.csv");
			BufferedReader reader = new BufferedReader(new FileReader(file));
			while ((line = reader.readLine()) != null) {
				hg.assertAtom(graph, getPerson(line));
			}
			reader.close();
			
			file = new File("/home/cata/Desktop/samplerel.csv");
			reader = new BufferedReader(new FileReader(file));
			while ((line = reader.readLine()) != null) {
				graph.add((HGValueLink) getRelation(line, graph));
			}
			reader.close();
//			
		} catch(IndexOutOfBoundsException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			System.out.println("The file seems to be missing."
					+ " Please double-check the pathname and try again.");
		} catch (IOException e) {
			System.out.println(e.getStackTrace());
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
		} finally {
			System.out.println("THE END");
			graph.close();
			System.exit(0);
		}
	}

	private static HGValueLink getRelation(String line, HyperGraph graph) {
		String[] relation = line.split(",");
		HGHandle handleP1 =  (HGHandle) graph.findOne(hg.and(hg.type(Person.class), hg.eq("name", relation[0])));
		HGHandle handleP2 =  (HGHandle) graph.findOne(hg.and(hg.type(Person.class), hg.eq("name", relation[1])));
		System.out.println(handleP1 + " " + handleP2);
		return new HGValueLink(relation[2], handleP1, handleP2);
	}

	/**
	 * 
	 * @param csvLine format: name,occupation,company
	 * @return Person with those attributes
	 */
	private static Person getPerson(String csvLine){
		String[] person = csvLine.split(",");
		return new Person(person[0], person[1], person[2]);
	}

}



//graph.add(new HGValueLink("friends", graph.add(getPerson("A,Y,Z")), graph.add(getPerson("Z,Y,A"))));
//graph.add(new HGValueLink("friends", graph.add(getPerson("A,B,C")), graph.add(getPerson("C,B,A"))));
//graph.add(new HGValueLink("friends", graph.add(getPerson("D,E,F")), graph.add(getPerson("F,E,D"))));
//final LinkProjectionMapping map = new LinkProjectionMapping(0);
//DerefMapping map2 = new DerefMapping(graph);
//CompositeMapping<HGLink, Object> bigmap = new CompositeMapping(map,map2);
//final AtomTypeCondition condition = new AtomTypeCondition(Person.class);
//MapCondition mapCondition = new MapCondition(condition, map) {
//	@Override
//	public boolean satisfies(HyperGraph hg, HGHandle handle) {
////		return true;
//		try {
//		return condition.satisfies(hg, map.eval((HGValueLink) hg.get(handle)));
//		} catch (Exception e) {
//			return true;
//		}
//	};
//};
//List<Person> as = hg.getAll(graph, hg.and(hg.type(Person.class), hg.eq("name", "A")));
//AtomValueCondition relationTypeCondition = new AtomValueCondition("friends", ComparisonOperator.EQ);
//System.out.println(graph.count(relationTypeCondition));
//for (HGHandle one: graph.findAll(relationTypeCondition)) {
//	if (as.contains((Person) graph.get(map.eval((HGValueLink) graph.get(one))))) {
//		System.out.println("Found one");
//	}
//	HGValueLink link = (HGValueLink) graph.get(one);
//	System.out.println(((Person) bigmap.eval(link)).getName());
//	Person p1 = (Person) graph.get(link.getTargetAt(0));
//	Person p2 = (Person) graph.get(link.getTargetAt(1));
//	System.out.println(p1.getName() + "-" + p2.getName());
//}
//
//for (Person p: as) {
//	System.out.println(p.getOccupation());
//}