package qsx.project.main;

import java.io.IOException;
import java.net.BindException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.java.biz.source_code.miniTemplator.MiniTemplator;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.hypergraphdb.HGHandle;
import org.hypergraphdb.HGQuery.hg;
import org.hypergraphdb.HGValueLink;
import org.hypergraphdb.HyperGraph;
import org.hypergraphdb.query.AtomValueCondition;
import org.hypergraphdb.query.ComparisonOperator;
import org.hypergraphdb.query.HGQueryCondition;
import org.hypergraphdb.query.impl.LinkProjectionMapping;

import qsx.project.datatypes.Person;
import qsx.project.main.MinimalServlets.HelloServlet;

public class Main {
	

	private static final String path = "/home/cata/workspace/QSX/res/htmlTemplate/template";
	private static final String templateFileName = path + "/template.html";

	public static void main(String[] args) throws Exception {
		int PORT = 5000;
		boolean successful = false;
		while (!successful) {
			try {
				Server server = new Server();
				ServerConnector connector = new ServerConnector(server);
				connector.setPort(PORT);
				server.setConnectors(new Connector[] { connector });
				ServletContextHandler context = new ServletContextHandler();
				context.setContextPath("/");
				context.addServlet(HelloServlet.class, "/hello");
				context.addServlet(QueryFormServlet.class, "/queryform.html");
				context.addServlet(QueryServlet.class, "/query");
				context.addServlet(AddPersonFormServlet.class, "/addpersonform.html");
				context.addServlet(AddPersonServlet.class, "/addperson");
				context.addServlet(AddRelationFormServlet.class, "/addrelationform.html");
				context.addServlet(AddRelationServlet.class, "/addrelation");
				context.addServlet(BasicServlet.class, "/*");
				HandlerCollection handlers = new HandlerCollection();
				handlers.setHandlers(new Handler[] {context, new DefaultHandler()});
				server.setHandler(handlers);
				server.start();
				server.join();
				successful = true;
			} catch (BindException e) {
				PORT++;
			}
		}
	}

	@SuppressWarnings("serial")
	public static class QueryServlet extends HttpServlet {

		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			System.out.println("Received request: " + request.getRequestURL());
			String relationType = "friends";
			Map<String, String[]> parameters = request.getParameterMap();
			HGQueryCondition p1Condition = hg.type(Person.class);
			if (parameters.containsKey("name1") && parameters.get("name1")[0] != "") {
				p1Condition = hg.and(p1Condition, hg.eq("name", parameters.get("name1")[0]));
			}
			if (parameters.containsKey("occupation1") && parameters.get("occupation1")[0] != "") {
				p1Condition = hg.and(p1Condition, hg.eq("occupation", parameters.get("occupation1")[0]));
			}
			if (parameters.containsKey("company1") && parameters.get("company1")[0] != "") {
				System.out.println(parameters.get("company1")[0] != "");
				p1Condition = hg.and(p1Condition, hg.eq("company", parameters.get("company1")[0]));
			}
			HGQueryCondition p2Condition = hg.type(Person.class);
			if (parameters.containsKey("name2") && parameters.get("name2")[0] != "") {
				p2Condition = hg.and(p2Condition, hg.eq("name", parameters.get("name2")[0]));
			}
			if (parameters.containsKey("occupation2") && parameters.get("occupation2")[0] != "") {
				p2Condition = hg.and(p2Condition, hg.eq("occupation", parameters.get("occupation2")[0]));
			}
			if (parameters.containsKey("company2") && parameters.get("company2")[0] != "") {
				p2Condition = hg.and(p2Condition, hg.eq("company", parameters.get("company2")[0]));
			}
			final LinkProjectionMapping map1 = new LinkProjectionMapping(0);
			final LinkProjectionMapping map2 = new LinkProjectionMapping(1);
			if (parameters.containsKey("relType") && parameters.get("relType")[0] != "") {
				relationType = parameters.get("relType")[0];
			}
			String htmlList = "<ol>\n";
			HyperGraph graph = new HyperGraph("/home/cata/development/qsx/gdb");
			try {
				List<Person> group1 = hg.getAll(graph, p1Condition);
				List<Person> group2 = hg.getAll(graph, p2Condition);
				AtomValueCondition relationTypeCondition = new AtomValueCondition(relationType, ComparisonOperator.EQ);
				for (HGHandle one: graph.findAll(relationTypeCondition)) {
					Person person1 = (Person) graph.get(map1.eval((HGValueLink) graph.get(one)));
					Person person2 = (Person) graph.get(map2.eval((HGValueLink) graph.get(one)));
					if (group1.contains(person1) && group2.contains(person2)) {
						htmlList += "<li>" + person1.getName() + " - " + person2.getName() + "</li>\n";
					}
				}
				htmlList += "</ol>\n";
			}finally {
				graph.close();
			}
			MiniTemplator templator = new MiniTemplator(templateFileName);
			templator.setVariable("title", "Query Result");
			templator.setVariable("body", htmlList);
			response.setContentType("text/html");
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().println(templator.generateOutput());
		}
	}
	

	@SuppressWarnings("serial")
	public static class AddPersonServlet extends HttpServlet {

		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			System.out.println("Received request: " + request.getRequestURL());
			String outcome = "Your record has been successfully added to the database";
			Map<String, String[]> parameters = request.getParameterMap();
			if (!parameters.containsKey("name")
					|| !parameters.containsKey("occupation")
					|| !parameters.containsKey("company")) {
				outcome = "Please fill all fields and try again.";
			} else {
				HyperGraph graph = new HyperGraph("/home/cata/development/qsx/gdb");
				try {
					hg.assertAtom(graph, new Person(parameters.get("name")[0], parameters.get("occupation")[0],
							parameters.get("company")[0]));
				}finally {
					graph.close();
				}
			}
			MiniTemplator templator = new MiniTemplator(templateFileName);
			templator.setVariable("title", "Import Result");
			templator.setVariable("body", outcome);
			response.setContentType("text/html");
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().println(templator.generateOutput());
		}
	}
	
	@SuppressWarnings("serial")
	public static class AddRelationServlet extends HttpServlet {

		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			System.out.println("Received request: " + request.getRequestURL());
			String relationType = "friends";
			Map<String, String[]> parameters = request.getParameterMap();
			HGQueryCondition p1Condition = hg.type(Person.class);
			if (parameters.containsKey("name1")) {
				p1Condition = hg.and(p1Condition, hg.eq("name", parameters.get("name1")[0]));
			}
			if (parameters.containsKey("occupation1")) {
				p1Condition = hg.and(p1Condition, hg.eq("occupation", parameters.get("occupation1")[0]));
			}
			if (parameters.containsKey("company1")) {
				p1Condition = hg.and(p1Condition, hg.eq("company", parameters.get("company1")[0]));
			}
			HGQueryCondition p2Condition = hg.type(Person.class);
			if (parameters.containsKey("name2")) {
				p2Condition = hg.and(p2Condition, hg.eq("name", parameters.get("name2")[0]));
			}
			if (parameters.containsKey("occupation2")) {
				p2Condition = hg.and(p2Condition, hg.eq("occupation", parameters.get("occupation2")[0]));
			}
			if (parameters.containsKey("company2")) {
				p2Condition = hg.and(p2Condition, hg.eq("company", parameters.get("company2")[0]));
			}
			final LinkProjectionMapping map1 = new LinkProjectionMapping(0);
			final LinkProjectionMapping map2 = new LinkProjectionMapping(1);
			if (parameters.containsKey("relType")) {
				relationType = parameters.get("relType")[0];
			}
			String htmlList = "<ol>\n";
			HyperGraph graph = new HyperGraph("/home/cata/development/qsx/gdb");
			try {
				List<Person> group1 = hg.getAll(graph, p1Condition);
				List<Person> group2 = hg.getAll(graph, p2Condition);
				AtomValueCondition relationTypeCondition = new AtomValueCondition(relationType, ComparisonOperator.EQ);
				for (HGHandle one: graph.findAll(relationTypeCondition)) {
					Person person1 = (Person) graph.get(map1.eval((HGValueLink) graph.get(one)));
					Person person2 = (Person) graph.get(map2.eval((HGValueLink) graph.get(one)));
					if (group1.contains(person1) && group2.contains(person2)) {
						htmlList += "<li>" + person1.getName() + " - " + person2.getName() + "</li>\n";
					}
				}
				htmlList += "</ol>\n";
			}finally {
				graph.close();
			}
			MiniTemplator templator = new MiniTemplator(templateFileName);
			templator.setVariable("title", "Query Result");
			templator.setVariable("body", htmlList);
			response.setContentType("text/html");
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().println(templator.generateOutput());
		}
	}
	
	@SuppressWarnings("serial")
	public static class AddPersonFormServlet extends HttpServlet {

		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			MiniTemplator templator = new MiniTemplator(path + "/addpersonform.html");
			response.setContentType("text/html");
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().println(templator.generateOutput());
		}
	}
	
	@SuppressWarnings("serial")
	public static class AddRelationFormServlet extends HttpServlet {

		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			MiniTemplator templator = new MiniTemplator(path + "/addrelationform.html");
			response.setContentType("text/html");
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().println(templator.generateOutput());
		}
	}
	
	@SuppressWarnings("serial")
	public static class QueryFormServlet extends HttpServlet {

		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			MiniTemplator templator = new MiniTemplator(path + "/queryform.html");
			response.setContentType("text/html");
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().println(templator.generateOutput());
		}
	}
	
	@SuppressWarnings("serial")
	public static class BasicServlet extends HttpServlet {

		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			MiniTemplator templator = new MiniTemplator(templateFileName);
			templator.setVariable("title", "");
			templator.setVariable("body", "");
			response.setContentType("text/html");
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().println(templator.generateOutput());
		}
	}
}
