package qsx.project.main;

import java.io.IOException;
import java.net.BindException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletHandler;

public class MinimalServlets {

	public static void main(String[] args) throws Exception {
		int PORT = 5000;
		boolean successful = false;
		while (!successful) {
			try {
				Server server = new Server(PORT);
				ServletHandler handler = new ServletHandler();
				server.setHandler(handler);
				handler.addServletWithMapping(HelloServlet.class, "/*");
				server.start();
				server.join();
				System.out.println("Server started at port: " + PORT);
				successful = true;
			} catch (BindException e) {
				PORT++;
			}
		}
    }
 
    @SuppressWarnings("serial")
    public static class HelloServlet extends HttpServlet {
 
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        	System.out.println("Received request: " + request.getRequestURL());
        	System.out.println(request.getParameterMap().toString());
            response.setContentType("text/html");
            response.setStatus(HttpServletResponse.SC_OK);
            response.getWriter().println("<h1>Hello SimpleServlet</h1>");
        }
    }
}