package qsx.project.datatypes;

public class Person {

	private String name;
	private String occupation;
	private String company;
	
	public Person() {
		
	}
	
	public Person(String name, String occupation, String company) {
		this.name = name;
		this.occupation = occupation;
		this.company = company;
	}

	public String getName() {
		return name;
	}

	public String getOccupation() {
		return occupation;
	}

	public String getCompany() {
		return company;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public void setCompany(String company) {
		this.company = company;
	}
	
	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Person)) {
			return false;
		}
		Person other = (Person) object;
		return this.name.equals(other.name)
				&& this.occupation.equals(other.occupation)
				&& this.company.equals(other.company);
	}
}
